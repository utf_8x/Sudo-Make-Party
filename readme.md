``` 
   ___ _   _  __| | ___    _ __ ___   __ _| | _____   _ __   __ _ _ __| |_ _   _ 
  / __| | | |/ _` |/ _ \  | '_ ` _ \ / _` | |/ / _ \ | '_ \ / _` | '__| __| | | |
  \__ \ |_| | (_| | (_) | | | | | | | (_| |   <  __/ | |_) | (_| | |  | |_| |_| |
  |___/\__,_|\__,_|\___/  |_| |_| |_|\__,_|_|\_\___| | .__/ \__,_|_|   \__|\__, |
                                                   |_|                      |___/ 
```

## Requirements:
* figlet

## Running this script:
* ```sudo make party``` 

## Compatible devices:
* ThinkPad X220 (Tested)
* Any other ThinkPad with the ThinkLight (untested)
